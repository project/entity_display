<?php

/**
 * @file
 * EntityDisplay Class definition.
 */

/**
 * Class EntityDisplay.
 *
 * Handle the entity display configuration of the platform to substitute the
 * standard Manage display UI of Drupal.
 */
class EntityDisplay {

  /*
   * PRIVATE ATTRIBUTES.
   */

  /**
   * Contains the display configuration for entities.
   *
   * Define visible elements and their render configuration.
   *
   * @var mixed
   */
  protected $entityDisplayConfs = array();
  /**
   * Contains the definition of each render configuration.
   *
   * They can be render arrays ('element' key) or field formatter configuration
   * options ('field' key).
   *
   * @var mixed
   */
  protected $renderElements = array();

  /**
   * Contains list of entities using the default display.
   *
   * @var array
   */
  protected $defaultDisplay = NULL;

  /*
   * PUBLIC METHODS.
   */

  /**
   * Load the render configurations.
   *
   * A rendering configuration define how an element is display.
   */
  public function initRenderElements() {
    global $theme_key;

    // Try from cache.
    if ($cache = cache_get('entity_display_render_elements', 'entity_display_cache')) {
      $global_elements = $cache->data;
      // Try to get the render elements information for the active theme.
      if (isset($global_elements[$theme_key])) {
        $init_elements = $global_elements[$theme_key];
      }
    }
    if (!isset($init_elements)) {
      $init_elements['element'] = $init_elements['field'] = array();
      // Load the render configurations defined by:
      //
      // - Modules through hook_entity_display_render_element:
      $render_elements =& $init_elements['element'];
      foreach (module_implements('entity_display_render_element') as $module) {
        // Use this instead of module_invoke to pass first parameter by
        // reference.
        $function = "{$module}_entity_display_render_element";
        $function($render_elements);
      }

      // - By themes using the custom DisplayDefinition system
      // (RenderElementDefinition instance).
      $render_element_definition = new RenderElementDefinition();
      foreach ($render_element_definition->functionsImplementsDefinition() as $function) {
        $function($render_elements);
      }

      // Load the field formatters defined by:
      //
      // - Modules through hook_entity_display_field_formatter:
      $field_formatters =& $init_elements['field'];
      foreach (module_implements('entity_display_field_formatter') as $module) {
        $function = "{$module}_entity_display_field_formatter";
        $function($field_formatters);
      }

      // - By themes using the custom DisplayDefinition system
      // (FieldFormatterDefinition instance).
      $context['formatter_info'] = $context = array();
      $field_types = _field_info_collate_types();
      foreach ($field_types['formatter types'] as $formatter_type => $formatter_settings) {
        $formatter_module = $formatter_settings['module'];
        if (!isset($context['formatter_info'][$formatter_module])) {
          $context['formatter_info'][$formatter_module] = array();
        }
        array_push($context['formatter_info'][$formatter_module], $formatter_type);
      }
      // Pass as context the list of existing field formatters broken down by
      // module.
      $field_formatter_definition = new FieldFormatterDefinition();
      foreach ($field_formatter_definition->functionsImplementsDefinition($context) as $function) {
        $function($field_formatters);
      }
      // Store the render elements info in cache for the active theme.
      $global_elements[$theme_key] = $init_elements;
      cache_set('entity_display_render_elements', $global_elements, 'entity_display_cache');
    }
    $this->renderElements = $init_elements;
  }

  /**
   * Retrieves the loaded render configurations.
   *
   * @return mixed
   *   Render configurations.
   */
  public function getConfElements() {

    return $this->renderElements;
  }

  /**
   * Get a loaded render configuration for a specific element.
   *
   * @param string $element_conf
   *   The configuration element name.
   * @param string $type
   *   The element configuration type: 'element' or 'field'.
   *
   * @return mixed
   *   The render configuration or FALSE in case it doesn't exist.
   */
  public function getConfElement($element_conf, &$type) {

    $conf_types = array(
      'field',
      'element',
    );
    foreach ($conf_types as $conf_type) {
      if (isset($this->renderElements[$conf_type][$element_conf])) {
        $type = $conf_type;
        return $this->renderElements[$conf_type][$element_conf];
      }
    }

    return FALSE;
  }

  /**
   * Set the display configuration for an entity.
   *
   * @param string $entity_type
   *   Type of the entity.
   * @param string $bundle
   *    Bundle of the entity.
   * @param string $view_mode
   *   View mode of the entity.
   * @param mixed $display_conf
   *   Display configuration to be set.
   */
  public function setEntityDisplayConf($entity_type, $bundle, $view_mode, $display_conf) {

    $this->entityDisplayConfs[$entity_type][$bundle][$view_mode] = $display_conf;
  }

  /**
   * Check if an entity uses the default display.
   *
   * By default, this options is disabled. It can be modified through the
   * variable 'entity_display_default_display' or the function
   * 'hook_entity_display_default_display_alter'.
   *
   * @param string $entity_type
   *   Type of the entity.
   * @param string $bundle
   *    Bundle of the entity.
   * @param string $view_mode
   *   View mode of the entity.
   *
   * @return bool
   *   TRUE if the entity use the default display, FALSE otherwise.
   */
  public function useDefaultDisplay($entity_type, $bundle, $view_mode) {
    // Check the object property (this works as static cache).
    $default_display =& $this->defaultDisplay;
    if (!isset($default_display)) {
      $default_display = variable_get('entity_display_default_display', array());
      drupal_alter('entity_display_default_display', $default_display);
    }

    return !empty($default_display[$entity_type][$bundle][$view_mode]);
  }

  /**
   * Get the display configuration for an entity.
   *
   * @param string $entity_type
   *   Type of the entity.
   * @param string $bundle
   *   Bundle of the entity.
   * @param string $view_mode
   *   View mode of the entity.
   *
   * @return mixed
   *    The display configuration or FALSE if it doesn't exist.
   */
  public function getEntityDisplayConf($entity_type, $bundle, $view_mode) {
    global $theme_key;

    $view_mode = $this->useDefaultDisplay($entity_type, $bundle, $view_mode) ? 'default' : $view_mode;
    // If it hasn't been previously processed in this request. This works as a
    // static cache.
    if (!isset($this->entityDisplayConfs[$entity_type][$bundle][$view_mode])) {
      $entities_conf = array();

      // Try from cache.
      if ($cache = cache_get('entity_display_entity_display_confs', 'entity_display_cache')) {
        $entities_conf = $cache->data;
        if (isset($cache->data[$theme_key][$entity_type][$bundle][$view_mode])) {
          $display_conf = $cache->data[$theme_key][$entity_type][$bundle][$view_mode];
        }
      }

      // In there is no luck, gather the displays defined:
      if (!isset($display_conf)) {
        $display_conf = array();
        // - By modules through hook_entity_display.
        foreach (module_implements('entity_display') as $module) {
          $function = "{$module}_entity_display";
          $function($display_conf, $entity_type, $bundle, $view_mode);
        }

        // - By themes using the custom DisplayDefinition system
        // (EntityDefinition instance).
        $entity_definition = new EntityDefinition();
        $context = array(
          'entity_type' => $entity_type,
          'view_mode' => $view_mode,
          'bundle' => $bundle,
        );
        foreach ($entity_definition->functionsImplementsDefinition($context) as $function) {
          $function($display_conf, $entity_type, $bundle, $view_mode);
        }

        $entities_conf[$theme_key][$entity_type][$bundle][$view_mode] = $display_conf;
        cache_set('entity_display_entity_display_confs', $entities_conf, 'entity_display_cache');
      }

      $this->setEntityDisplayConf($entity_type, $bundle, $view_mode, $display_conf);
    }

    return $this->entityDisplayConfs[$entity_type][$bundle][$view_mode];
  }

  /**
   * Set the display for an entity from the entity display configuration.
   *
   * @param object $entity
   *   The entity which the display will be attached.
   * @param string $entity_type
   *   Entity type.
   * @param mixed $entity_display_conf
   *    The display configuration of the entity.
   */
  public function setEntityDisplay(&$entity, $entity_type, $entity_display_conf) {

    // Create the display for each element in the entity configuration. The
    // display always must result in a render array.
    foreach ($entity_display_conf as $element_name => $element_conf) {

      // Extra fields are treated differently by Drupal, the display of these
      // fields are generated and included through the implementation of
      // hook_field_extra_fields_display_alter (see setEntityDisplayExtraFields)
      if ($element_conf == 'entity_display_extra_field') {
        continue;
      }

      $render_conf = $this->getConfElement($element_conf, $type);
      if (!$render_conf) {
        $message_variables = array(
          '@element_name' => $element_name,
          '@element_conf' => $element_conf,
          '@entity_type' => $entity_type,
          '@bundle' => isset($entity->content['#bundle']) ? $entity->content['#bundle'] : '',
        );
        drupal_set_message(t("Entity display: The render configuration '@element_conf' wasn't found for the element '@element_name' for '@entity_type'/'@bundle'", $message_variables), 'error');
        continue;
      }
      // Display for custom render elements.
      if ($type == 'element') {
        // "Customize" the render options including the entity, our custom theme
        // elements will need it!
        $render_conf += array(
          '#entity' => clone $entity,
          '#entity_display_element_name' => $element_name,
        );

        // Allow other modules to alter render element before including it.
        // Using the property access they can avoid the element being rendered.
        drupal_alter('entity_display_render_element', $render_conf);
        $entity->content[$element_name] = $render_conf;
      }
      // Display for fields.
      elseif ($type == 'field') {
        // Create the render array using the defined formatter.
        if (!empty($entity->{$element_name})) {
          $render_array = field_view_field($entity_type, $entity, $element_name, $render_conf);
          $entity->content[$element_name] = $render_array;
        }
      }
    }
  }

  /**
   * Include extra fields in the entity display.
   *
   * @param array $displays
   *    The current display of the entity.
   * @param array $entity_display_conf
   *    The display configuration of the entity.
   */
  public function setEntityDisplayExtraFields(&$displays, $entity_display_conf) {
    foreach ($entity_display_conf as $element_name => $element_conf) {
      if ($element_conf == 'entity_display_extra_field') {
        $displays[$element_name]['visible'] = TRUE;
      }
    }
  }

}
