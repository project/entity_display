<?php

/**
 * @file
 * RenderElementDefinition Class definition.
 */

/**
 * Class RenderElementDefinition.
 *
 * Get and manage field formatter definitions in a theme level.
 */
class RenderElementDefinition extends DisplayDefinition {

  /**
   * RenderElementDefinition constructor.
   */
  public function __construct() {
    $this->fileType   = 'render';
    $this->rootFolder = 'display_configuration/element_display/render_elements';
    $this->hook       = 'entity_display_render_element';
  }

  /**
   * Implements suggested names to define render element displays.
   *
   * In this case the suggestions are defined with an empty list. It means only
   * the default definition could be used.
   */
  protected function getDefinitionSuggestions($context = array()) {

    $theme_suggestions = array();
    drupal_alter('render_element_definition_suggestions', $theme_suggestions, $context);

    return $theme_suggestions;
  }

  /**
   * Extends the method functionsImplementsDefinition.
   *
   * It sets the context argument to an empty value by default.
   */
  public function functionsImplementsDefinition($context = array()) {
    return parent::functionsImplementsDefinition($context);
  }

}
