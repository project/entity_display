<?php

/**
 * @file
 * FieldFormatterDefinition Class definition.
 */

/**
 * Class FieldFormatterDefinition.
 *
 * Get and manage field formatter definitions in a theme level.
 */
class FieldFormatterDefinition extends DisplayDefinition {

  /**
   * FieldFormatterDefinition constructor.
   */
  public function __construct() {
    $this->fileType   = 'formatter';
    $this->rootFolder = 'display_configuration/element_display/field_formatters';
    $this->hook       = 'entity_display_field_formatter';
  }

  /**
   * Implements the suggested names to define field formatters.
   *
   * The suggestions are based in the module which created the field formatter
   * and its type.
   *
   * E.g.
   * ---
   * - The suggestion "text" => "text_default" will look for a function called
   *   <theme_name>_field_formatter_text_default in a file named
   *   "text.formatter".
   */
  protected function getDefinitionSuggestions($context) {

    $formatter_info = $context['formatter_info'];
    $theme_suggestions = $formatter_info;
    drupal_alter('field_formatter_definition_suggestions', $theme_suggestions, $context);

    return $theme_suggestions;
  }

}
