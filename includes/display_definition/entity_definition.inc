<?php

/**
 * @file
 * EntityDefinition Class definition.
 */

/**
 * Class EntityDefinition.
 *
 * Get and manage the display definition of entities in a theme level.
 */
class EntityDefinition extends DisplayDefinition {

  /**
   * EntityDefinition constructor.
   */
  public function __construct() {
    $this->fileType   = 'display';
    $this->rootFolder = 'display_configuration/entity_display';
    $this->hook       = 'entity_display';
  }

  /**
   * Implements the suggested names to define entity displays.
   *
   * By default, the suggestions are based in entity_type, view_mode, bundle
   * in the following order (files and functions follow the same pattern).
   *  - <entity_type>.
   *  - <entity_type>_<view_mode>.
   *  - <entity_type>_<view_mode>_<bundle>.
   *
   * E.g:
   * ---
   *  - The suggestion "node" => "node" will look for a function called
   *   <theme_name>_entity_display_node in a file named "node.display".
   *
   * - The suggestion "user_teaser" => "user_teaser" will look for a function
   *   called <theme_name>_entity_display_user_teaser in a file named
   *   "user_teaser.display".
   */
  protected function getDefinitionSuggestions($context) {

    $entity_type = $context['entity_type'];
    $view_mode = $context['view_mode'];
    $bundle = $context['bundle'];

    $theme_suggestions = array(
      $entity_type => array($entity_type),
      $entity_type . '--' . $view_mode => array($entity_type . '_' . $view_mode),
      $entity_type . '--' . $view_mode . '--' . $bundle => array($entity_type . '_' . $view_mode . '_' . $bundle),
    );

    // Allow other modules to change the definition suggestion for an entity.
    // This could be specially useful to group entity definitions by other
    // type of relations (e.g. landing pages).
    drupal_alter('entity_display_definition_suggestions', $theme_suggestions, $context);

    return $theme_suggestions;
  }

}
