<?php

/**
 * @file
 * DisplayDefinition Class definition.
 */

/**
 * Class DisplayDefinition.
 *
 * Get and manage display related definitions in a theme level.
 */
abstract class DisplayDefinition {

  /**
   * Extension used in the files containing the display definition.
   *
   * @var string
   */
  protected $fileType;
  /**
   * Folder path where the definition files will be located in the theme.
   *
   * @var string
   */
  protected $rootFolder;
  /**
   * Name of the hook function used to create definitions.
   *
   * @var string
   */
  protected $hook;

  /**
   * Get recursively all the "$fileType" files located in a folder.
   *
   * @param string $dir
   *   Folder path.
   * @param array $results
   *   Current folder content, to be added to the next function call. Needed
   *   for the recursion to store intermediate results of the function.
   *
   * @return array
   *    List of files (key) and respective paths (value) in the folder.
   */
  protected function getDirContent($dir, &$results = array()) {

    $files = scandir($dir);

    foreach ($files as $file_name) {
      $file_path = realpath($dir . DIRECTORY_SEPARATOR . $file_name);
      $file_info = pathinfo($file_path);
      // Add just files with the definition type extension.
      if (!is_dir($file_path)) {
        if ($file_info['extension'] == $this->fileType) {
          $results[$file_info['filename']] = $file_info['dirname'];
        }
      }
      // If it is a directory, call the function recursively.
      elseif ($file_name != "." && $file_name != "..") {
        DisplayDefinition::getDirContent($file_path, $results);
      }
    }

    return $results;
  }

  /**
   * Get all the files implementing display definitions.
   *
   * The search is performed in the active theme (and base themes). Files must
   * be located in the folder "$rootFolder" of the theme root folder. Any
   * structure inside it is allowed.
   *
   * @return array
   *   List of display definition files broken down by theme.
   */
  protected function getDefinitionFiles() {

    global $theme, $base_theme_info;
    $definition_files = array();

    if (isset($theme)) {
      $theme_keys = array();
      foreach ($base_theme_info as $base) {
        $theme_keys[] = $base->name;
      }
      $theme_keys[] = $theme;
      foreach ($theme_keys as $theme_key) {
        $theme_path = DRUPAL_ROOT . '/' . drupal_get_path('theme', $theme_key) . "/{$this->rootFolder}";
        if (file_exists($theme_path)) {
          $definition_files[$theme_key] = DisplayDefinition::getDirContent($theme_path);
        }
      }
    }

    return $definition_files;
  }

  /**
   * Get suggested names to create display definitions.
   *
   * - Naming suggestions are used to find files and functions containing
   * display definitions.
   *
   * - Display definitions are overridden following the same order i.e. the last
   * being found has the highest priority.
   *
   * @param array $context
   *   Context data to create meaningful suggestions/definition structure.
   *
   * @return array
   *    List of definition suggestions. Each element of the array points out
   *    the suggested name/s for a definition file (key) and functions used in
   *    that file (value).
   */
  abstract protected function getDefinitionSuggestions($context);

  /**
   * Get actual functions implementing display definitions.
   *
   * - The calculation is based on its definition suggestions and the default
   *   definition: <theme>_<hook> in the file <hook>.<file_type>.
   *
   * - The definition functions found must be implemented following the
   *   structure defined by <$hook>.
   *
   * @param array $context
   *   Context data to create meaningful suggestions/definition structure.
   *
   * @return array
   *    List of functions defining the display.
   */
  public function functionsImplementsDefinition($context) {

    $display_functions = array();

    // Get all the files defining entity displays.
    $entity_definition_files = $this->getDefinitionFiles();
    // Get the naming suggestions for the entity display definitions.
    $definition_suggestions = $this->getDefinitionSuggestions($context);

    // Include as suggestion a base definition file. common to any definition
    // type, allowing the implementation of <theme>_<hook> in the file
    // <hook>.<file_type>.
    if (!isset($definition_suggestions[$this->hook])) {
      $definition_suggestions[$this->hook] = array('');
    }

    // Check what files and, therefore, functions are implementing definition
    // elements for the entity display.
    foreach ($definition_suggestions as $file_suggestion => $function_suggestions) {
      // Ensure any file make use of dashed instead of underscores to separate
      // words.
      $file_suggestion = self::normalizeFileSuggestion($file_suggestion);
      foreach ($entity_definition_files as $theme_key => $theme_definition_files) {
        if (isset($theme_definition_files[$file_suggestion])) {
          $folder_path = $theme_definition_files[$file_suggestion];
          $file_path = $folder_path . '/' . $file_suggestion . ".{$this->fileType}";

          if (file_exists($file_path)) {
            include_once $file_path;
          }
          foreach ($function_suggestions as $function_suggestion) {
            $function = empty($function_suggestion) ? $theme_key . "_{$this->hook}" : $theme_key . "_{$this->hook}_" . $function_suggestion;
            if (function_exists($function)) {
              $display_functions[] = $function;
            }
          }
        }
      }
    }

    return $display_functions;
  }

  /**
   * Normalize suggestions to name file definitions.
   *
   * @param string $file_suggestion
   *   Suggested name for the file definition.
   *
   * @return string
   *   Normalized name.
   */
  public static function normalizeFileSuggestion($file_suggestion) {
    return str_replace('_', '-', $file_suggestion);
  }

}
