<?php

/**
 * @file
 * Entity display API.
 */

/*
 * MODULE HOOKS.
 *
 * Module-side implemented functions.
 */

/**
 * Define an entity display.
 *
 * The goal of this hook is to define how an entity is render:
 *   - Displayed fields. Only the included fields will be displayed.
 *   - Element definition. Specify how the elements are displayed, using
 *     field formatter and render element definitions defined by
 *     hook_entity_display_field_formatter() and
 *     hook_entity_display_render_element().
 *
 * @param array $display_elements
 *   Elements already included in the display. It is passed by reference to
 *   allow its modification.
 * @param string $entity
 *   Entity whose display is being created.
 * @param string $type
 *   Entity type (node, user, etc).
 * @param string $view_mode
 *   Display view mode.
 */
function hook_entity_display(&$display_elements, $entity, $type, $view_mode) {
  // This is an example how we could set the display of the view mode 'teaser'
  // for nodes of any type.
  if ($type == 'node' && $view_mode == 'teaser') {
    $display_elements = array(
      'title_field'        => 'module_name_smart_trimmer_title',
      'short_title'        => 'module_name_trimmer_10_title',
      'field_content_date' => 'module_name_date_time_picker',
      'field_brand_ref'    => 'module_name_entity_display_full',
    );
  }
  // Or how we could include the author biography in the display of nodes of
  // type 'article' (referencing the render element configuration 'user_bio').
  if ($type == 'node' && $view_mode == 'full') {
    $content_type = $entity->type;
    if ($content_type == 'article') {
      $display_elements['footer_author'] = 'module_name_user_bio';
    }
  }
  // Or how we could show the user full name through the render element
  // configuration 'user_full_name' in the view mode 'teaser'.
  if ($type == 'user' && $view_mode == 'teaser') {
    $display_elements['user_profile_full_name'] = 'module_name_user_full_name';
  }
  // This is as flexible as we want...
}

/**
 * Define field formatters to be used in hook_entity_display().
 *
 * Every formatter will follow the standard Drupal specification for field
 * displays:
 *  - 'type': The name of the formatter to use.
 *  - 'settings': The array of formatter settings.
 *  - 'label': The position of the field label or 'hidden'.
 *
 * @param array $formatters
 *   List of defined field formatters so far by other modules. The argument
 *   is passed by reference to allow other modules to override/unset values.
 */
function hook_entity_display_field_formatter(&$formatters) {
  // Field formatter to trim the field value (HTML) to 50 characters including
  // ellipsis.
  $formatters['module_name_smart_trimmer_title'] = array(
    'type' => 'smart_trim_format',
    'settings' => array(
      'trim_length' => 50,
      'trim_suffix' => '...',
    ),
  );

  // Field formatter to trim the field value (plain text) to 10 characters.
  $formatters['module_name_trimmer_10_title'] = array(
    'type' => 'trimmer',
    'settings' => array(
      'trim_length' => 10,
    ),
  );

  // This formatter creates the 'full' display of the referenced entity.
  $formatters['module_name_entity_display_full'] = array(
    'type' => 'entityreference_entity_view',
    'label' => 'above',
    'settings' => array(
      'links' => 0,
      'view_mode' => 'full',
    ),
  );
}

/**
 * Define render element configurations to be used in hook_entity_display().
 *
 * Every configuration will follow the standard Drupal specification of render
 * arrays:
 *  - '#theme': The name of the theme element.
 *  - '#cache': Cache settings.
 *  ...
 *
 * @param array $render_elements
 *   List of defined render elements so far by other modules. The argument
 *   is passed by reference to allow other modules to override/unset values.
 */
function hook_entity_display_render_element(&$render_elements) {
  $render_elements['module_name_user_bio'] = array(
    '#theme' => 'user_profile_biography',
    '#cache' => array(
      'keys' => array('module_name', 'user_bio'),
      'bin' => 'cache',
      'expire' => time() + 120,
      'granularity' => DRUPAL_CACHE_PER_PAGE,
    ),
  );

  $render_elements['module_name_user_full_name'] = array(
    '#theme' => 'user_profile_name',
    // No cache as this element can be included more that once in the same page
    // for different users.
  );
}

/**
 * Allow to alter entity display theme-definition suggestions.
 *
 * The suggestion system point out the location of entity display definitions
 * in the theme layer. Just the files/functions provided by the system will be
 * checked.
 *
 * The default suggestions follow the pattern (and priority):
 *
 *  1. File: <entity_type> - Function: <entity_type>
 *  2. File: <entity_type>--<view_mode> - Function: <entity_type>_<view_mode>
 *  3. File: <entity_type>--<view_mode>--<bundle> -
 *     Function: <entity_type>__<view_mode>_<bundle>
 *
 * @param array $theme_suggestions
 *   Definition suggestions:
 *     $theme_suggestions = array(
 *       '<definition_file_1>' = array(
 *         '<definition_function_1>',
 *         '<definition_function_2>',
 *         ...
 *       ),
 *       '<definition_file_2>' = array(
 *         ...
 *     ).
 * @param array $context
 *   Context of the entity to display:
 *     $context = array(
 *       'entity_type' => <entity_type>,
 *       'view_mode' => <view_mode>,
 *       'bundle' => <bundle>,
 *     ).
 */
function hook_entity_display_definition_suggestions_alter(&$theme_suggestions, $context) {
  // Group the common display elements of the content types article and blog
  // in the same file/function to avoid redundant definitions.
  $entity_type = $context['entity_type'];
  $view_mode   = $context['view_mode'];
  $bundle      = $context['bundle'];

  if ($entity_type == 'node' && ($bundle == 'article' || $bundle == 'blog')) {
    $theme_suggestions["{$entity_type}--{$view_mode}--publications"] = array("{$entity_type}_{$view_mode}_publications");
  }
}

/**
 * Allow to alter field formatter theme-definition suggestions.
 *
 * Again, the definition system points out where to find field formatter
 * definitions in the theme.
 *
 * By default: File: <formatter_module> - Functions: <formatter_type>
 *
 * @param array $theme_suggestions
 *   Definition suggestions:
 *     $theme_suggestions = array(
 *       '<definition_file_1>' = array(
 *         '<definition_function_1>',
 *         '<definition_function_2>',
 *         ...
 *       ),
 *       '<definition_file_2>' = array(
 *         ...
 *     ).
 * @param array $context
 *   Context of the defined field formatter:
 *     $context = array(
 *       'formatter_info' => array(
 *         '<formatter_module>' => array(
 *            <formatter_type_1>,
 *            <formatter_type_2>,
 *            ...
 *         ),
 *       ),
 *     ).
 */
function hook_field_formatter_definition_suggestions_alter(&$theme_suggestions, $context) {
  // Use a different file for each formatter type instead of grouping field
  // formatters by module.
  $theme_suggestions_type = array();
  foreach ($theme_suggestions['formatter_info'] as $formatter_module => $formatter_types) {
    foreach ($formatter_types as $formatter_type) {
      $theme_suggestions_type[$formatter_type] = array($formatter_type);
    }
  }
  $theme_suggestions = $theme_suggestions_type;
}

/**
 * Allow to alter render element theme-definition suggestions.
 *
 * Same, for render elements configurations. By default, no suggestions.
 *
 * @param array $theme_suggestions
 *   Definition suggestions:
 *     $theme_suggestions = array(
 *       '<definition_file_1>' = array(
 *         '<definition_function_1>',
 *         '<definition_function_2>',
 *         ...
 *       ),
 *       '<definition_file_2>' = array(
 *         ...
 *     ).
 * @param array $context
 *   Context of the defined render array:
 *     $context = array().
 */
function hook_render_element_definition_suggestions_alter(&$theme_suggestions, $context) {
  // Define render elements for custom theme elements in a separated file.
  $theme_suggestions['my_render_elements_file'] = array('my_render_elements_function');
}

/*
 * THEME HOOKS.
 *
 * Theme-side implemented functions to define display related elements: entity
 * displays, field formatters and render elements.
 */

/**
 * Define an entity display in the theme.
 */
function theme_entity_display_suggestion(&$display_elements, $entity, $type, $view_mode) {
  // Replace the field formatter used for field 'created' and remove the body
  // field from the node display in this theme.
  if ($type == 'node' && $view_mode == 'teaser') {
    $display_elements['created'] = 'theme_date_default';
    unset($display_elements['body']);
  }
  // ..
}

/**
 * Define field formatters (theme) to be used in hook_entity_display().
 */
function theme_entity_display_field_formatter_suggestion(&$formatters) {
  $formatters['theme_date_default'] = array(
    'type' => 'date_default',
    'settings' =>
      array(
        'format_type' => 'pp_short',
        'fromto' => 'both',
        'multiple_from' => '',
        'multiple_number' => '',
        'multiple_to' => '',
        'show_remaining_days' => FALSE,
      ),
  );
}

/**
 * Define render configurations (theme) to be used in hook_entity_display().
 */
function theme_entity_display_render_element_suggestion(&$render_elements) {
  // Increase the time the render element conf stays in cache to 2 minutes.
  $render_elements['module_name_user_bio']['#cache']['time'] = time() + 120;
}
